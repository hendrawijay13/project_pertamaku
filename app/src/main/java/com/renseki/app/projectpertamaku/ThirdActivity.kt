package com.renseki.app.projectpertamaku

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.renseki.app.projectpertamaku.model.Gender
import com.renseki.app.projectpertamaku.model.Mahasiswa
import kotlinx.android.synthetic.main.third_activity.*

class ThirdActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_MHS = "EXTRA_MHS"

        fun getStartIntent(
                context: Context,
                mahasiswa: Mahasiswa
        ) = Intent(context, ThirdActivity::class.java).apply {
            putExtra(EXTRA_MHS, mahasiswa)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.third_activity)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val mahasiswa = intent.getParcelableExtra<Mahasiswa>(EXTRA_MHS)

        nrp_field.text = mahasiswa.nrp
        name_field.text = mahasiswa.name
        gender_field.text = when(mahasiswa.gender) {
            Gender.MALE -> getString(R.string.male)
            Gender.FEMALE -> getString(R.string.female)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when(item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }
}